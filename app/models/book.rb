class Book < ApplicationRecord
  scope :ordered_by_id, -> { order(id: :asc) }
  validates :title, presence: true
end
